import java.sql.*;
import java.sql.Connection;
import java.sql.Driver;
/**
 * Created by Sergey Rewa on 05.04.2016.
 */
public class MyJDBC {
    private static final String USERNAME="root";
    private static final String PASSWORD="qazxsw";
    private static final String URL="jdbc:mysql://localhost:3306/user_database";
    public static void main(String[] args) throws SQLException {
        Connection conn=null;
        Driver driver;
        try {
            driver=new com.mysql.jdbc.Driver();
        }
        catch(SQLException e){
            System.out.println("Произошла ошибка при создании драйвера");
            return;
        }
        try {
            DriverManager.registerDriver(driver);
        }
        catch (SQLException e){
            System.out.println("Не удалось зарегистрировать драйвер");
            return;
        }
        try {
            conn= DriverManager.getConnection(URL,USERNAME,PASSWORD);
            Statement statement = conn.createStatement();
            //statement.execute("insert into user_database.new_table (user_name, user_age, group_id, user_skills) values(\"Serg\",37,3, \"Deer\")");
            //System.out.println("Succesfully updated");
            //statement.execute("update user_database.new_table set user_age=38 where user_id=7");
            //System.out.println("Succesfully changed");
            statement.execute("delete from user_database.new_table where user_id=7");
            System.out.println("Succesfully deleted");
        }
        catch (SQLException e)
        {
            System.out.println("Не удалось создать подключение");
            return;
        }
        finally {
            if (conn !=null)
                conn.close();
        }
    }
}


